import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observer, Observable } from 'rxjs';
import { Http, Response, Headers, RequestOptions, ResponseContentType } from '@angular/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BackendApiService {
  REST_API_URL = environment.REST_API_URL;
  // private REST_API_URL = 'http://localhost:7774/';

  constructor(private httpClient: HttpClient) { }


//api to get all the services
 getAllServices(){
    console.log("inside get all service api....");
    return this.httpClient.get('REST_API_URL');
  }


  //api to upload file

  pushFilesToStorage(formdata): Observable<any> {
    const formdataFile: FormData = new FormData();
    formdataFile.append('file', formdata, formdata.name);
    console.log(formdata);
    return this.httpClient.post(this.REST_API_URL + '/FileUpload/', formdata);
  }
}
