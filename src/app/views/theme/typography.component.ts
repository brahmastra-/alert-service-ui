import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { BackendApiService } from '../../services/service-api.service';

@Component({
  templateUrl: 'typography.component.html'
})
export class TypographyComponent implements OnInit {
  serviceInfo: FormGroup;
  statusForm: FormGroup;
  propertyUploadForm: FormGroup;
  // serviceInfo: FormGroup;

  private serviceName;
  private serviceCode ;
  private language;

  private selectedFiles: FileList;
  private fileNames: string;
  private selectedFile;
  @ViewChild('FileUpload', { static: true }) fileUpload: ElementRef;
  constructor( 
    private fb: FormBuilder,
    private backendApiService:BackendApiService
    ) 
    {
      this.serviceInfoCreateForm();
      this.serviceStatusCreateForm();
      this.servicePropertyUloadForm();
 
     }
 
  serviceInfoCreateForm() {
      this.serviceInfo = this.fb.group({
        serviceName: [''],
        serviceCode: [''],
        language: ['']
      });
    }

    serviceStatusCreateForm() {
      this.statusForm = this.fb.group({
        status: ['']
      });
    }

    servicePropertyUloadForm() {
      this.propertyUploadForm = this.fb.group({
        propertyData: ['']
      });
    }

  ngOnInit(): void {
    console.log("Inside add screen")
  }

  onInfoSubmit(event: Event) {
    console.log("Inside onInfo Submit buttom");

    console.log('Your form data : ', this.serviceInfo.value );
  }
  onPropertySubmit(event: Event) {
    console.log("Inside onProperty Submit buttom");
    console.log('Your form data : ', this.propertyUploadForm.value );
  }

  onFileUploadSubmit(event: Event) {
    console.log("Inside onFileUpload Submit buttom")
  }

  onStatusSubmit(event: Event) {
    console.log('Your form data : ', this.statusForm.value );
    console.log("Inside onStatus Submit buttom")
  }

  onAuthGenrate(event: Event) {
    console.log("Inside onAuthGenrate buttom")
  }

  public selectFile(event){
    this.selectedFiles = event.target.files;

    console.log("files", this.selectedFiles)
  }


  public upload(){
    console.log("Inside file upload buttom")
    
    const formdata: FormData = new FormData();
    for(let i = 0; i <  this.selectedFiles.length; i++){
      console.log("File NAme:.",this.selectedFiles.item(i).name );
      this.selectedFile = this.selectedFiles.item(i).name ;
      formdata.append('file', this.selectedFiles.item(i), this.selectedFiles.item(i).name);
    this.backendApiService.pushFilesToStorage(this.selectedFiles.item(i)).subscribe(
      entry => {
        this.fileUpload.nativeElement.value='';
        this.fileNames = entry;
        this.selectedFiles = null;
        },
      );
  }
}
  
}
