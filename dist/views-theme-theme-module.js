(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["views-theme-theme-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/views/theme/colors.component.html":
/*!*****************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/theme/colors.component.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\r\n    <div class=\"row\">\r\n        <div class=\"col-sm-6 col-lg-3\">\r\n          <div class=\"card text-white bg-warning\">\r\n            <div class=\"card-body pb-0\">\r\n              <div class=\"text-value\" style=\"text-align: center;\">10</div>\r\n              <div style=\"text-align: center;\">Total Services</div>\r\n            </div>\r\n          </div>\r\n        </div><!--/.col-->\r\n        <div class=\"col-sm-6 col-lg-3\">\r\n            <div class=\"card text-white badge-success\">\r\n              <div class=\"card-body pb-0\">\r\n                <div class=\"text-value\" style=\"text-align: center;\">7</div>\r\n                <div style=\"text-align: center;\">Total Active Services</div>\r\n              </div>\r\n            </div>\r\n          </div><!--/.col-->\r\n          <div class=\"col-sm-6 col-lg-3\">\r\n              <div class=\"card text-white badge-danger\">\r\n                <div class=\"card-body pb-0\">\r\n                  <div class=\"text-value\" style=\"text-align: center;\">3</div>\r\n                  <div style=\"text-align: center;\">Total Inactive Services </div>\r\n                </div>\r\n              </div>\r\n            </div><!--/.col-->\r\n            <div class=\"col-sm-6 col-lg-3\">\r\n                <div style=\"text-align: left;\">\r\n                    <div class=\"col-md-6\" >\r\n                    <button type=\"button\" class=\"btn btn-block btn-primary\" (click)=\"onAddService()\">Add Service</button>\r\n                    </div>\r\n                </div>\r\n              </div><!--/.col-->\r\n      </div><!--/.row-->\r\n  <!-- <div style=\"text-align: left;\">\r\n      <div class=\"col-md-2\" >\r\n      <button type=\"button\" class=\"btn btn-block btn-primary\" (click)=\"onAddService()\">Add Service</button>\r\n      </div>\r\n  </div> -->\r\n  <div class=\"row\">\r\n    <div class=\"col-lg-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <i class=\"fa fa-align-justify\"></i>Service List\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <table class=\"table table-bordered table-striped table-sm\">\r\n            <thead>\r\n              <tr>\r\n                <th>Username</th>\r\n                <th>Date registered</th>\r\n                <th>Role</th>\r\n                <th>Status</th>\r\n              </tr>\r\n            </thead>\r\n            <tbody>\r\n              <tr>\r\n                <td>Vishnu Serghei</td>\r\n                <td>2012/01/01</td>\r\n                <td>Member</td>\r\n                <td>\r\n                  <span class=\"badge badge-success\">Active</span>\r\n                </td>\r\n              </tr>\r\n              <tr>\r\n                <td>Zbyněk Phoibos</td>\r\n                <td>2012/02/01</td>\r\n                <td>Staff</td>\r\n                <td>\r\n                  <span class=\"badge badge-danger\">Banned</span>\r\n                </td>\r\n              </tr>\r\n              <tr>\r\n                <td>Einar Randall</td>\r\n                <td>2012/02/01</td>\r\n                <td>Admin</td>\r\n                <td>\r\n                  <span class=\"badge badge-secondary\">Inactive</span>\r\n                </td>\r\n              </tr>\r\n              <tr>\r\n                <td>Félix Troels</td>\r\n                <td>2012/03/01</td>\r\n                <td>Member</td>\r\n                <td>\r\n                  <span class=\"badge badge-warning\">Pending</span>\r\n                </td>\r\n              </tr>\r\n              <tr>\r\n                <td>Aulus Agmundr</td>\r\n                <td>2012/01/21</td>\r\n                <td>Staff</td>\r\n                <td>\r\n                  <span class=\"badge badge-success\">Active</span>\r\n                </td>\r\n              </tr>\r\n            </tbody>\r\n          </table>\r\n          <nav>\r\n            <ul class=\"pagination\">\r\n              <li class=\"page-item\"><a class=\"page-link\" href=\"#\">Prev</a></li>\r\n              <li class=\"page-item active\">\r\n                <a class=\"page-link\" href=\"#\">1</a>\r\n              </li>\r\n              <li class=\"page-item\"><a class=\"page-link\" href=\"#\">2</a></li>\r\n              <li class=\"page-item\"><a class=\"page-link\" href=\"#\">3</a></li>\r\n              <li class=\"page-item\"><a class=\"page-link\" href=\"#\">4</a></li>\r\n              <li class=\"page-item\"><a class=\"page-link\" href=\"#\">Next</a></li>\r\n            </ul>\r\n          </nav>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/theme/typography.component.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/theme/typography.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\r\n\r\n<div class=\"row\">\r\n    <div class=\"col-md-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <strong>Service Info</strong>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <form  [formGroup]=\"serviceInfo\" class=\"form-horizontal\">\r\n            <!-- <div class=\"form-group row\">\r\n              <label class=\"col-md-3 col-form-label\">Static</label>\r\n              <div class=\"col-md-9\">\r\n                <p class=\"form-control-static\">Username</p>\r\n              </div>\r\n            </div> -->\r\n            <div class=\"form-group \">\r\n              <label class=\"col-md-2 col-form-label\" for=\"serviceName\">Service Name</label>\r\n              <div class=\"col-md-5\">\r\n                <input type=\"text\" id=\"serviceName\" name=\"serviceName\"  formControlName=\"serviceName\" class=\"form-control\" placeholder=\"Enter Service Name\">\r\n                <!-- <span class=\"help-block\">This is a help text</span> -->\r\n              </div>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label class=\"col-md-2 col-form-label\" for=\"email-input\">Service Code</label>\r\n              <div class=\"col-md-5\">\r\n                <input type=\"text\" id=\"serviceCode\" name=\"serviceCode\"  formControlName=\"serviceCode\" class=\"form-control\" placeholder=\"Enter Service Code\" autocomplete=\"email\">\r\n                <!-- <span class=\"help-block\">Please enter your email</span> -->\r\n              </div>\r\n            </div>\r\n            <div class=\"form-group \">\r\n                <!-- <div class=\"form-group col-sm-3\"> -->\r\n                  <label class=\"col-md-2 col-form-label\" for=\"ccmonth\">Language</label>\r\n                  <div class=\"col-md-5\">\r\n                  <select class=\"form-control\" name=\"serviceName\" formControlName=\"language\" id=\"language\">\r\n                    <option>JAVA</option>\r\n                  </select>\r\n                  </div>\r\n                <!-- </div> -->\r\n              </div><!--/.row-->\r\n            <!-- <div class=\"form-group row\">\r\n              <label class=\"col-md-3 col-form-label\" for=\"password-input\">Password</label>\r\n              <div class=\"col-md-9\">\r\n                <input type=\"password\" id=\"password-input\" name=\"password-input\" class=\"form-control\" placeholder=\"Password\" autocomplete=\"current-password\">\r\n                <span class=\"help-block\">Please enter a complex password</span>\r\n              </div>\r\n            </div>\r\n            <div class=\"form-group row\">\r\n              <label class=\"col-md-3 col-form-label\" for=\"date-input\">Date Input</label>\r\n              <div class=\"col-md-9\">\r\n                <input class=\"form-control\" id=\"date-input\" type=\"date\" name=\"date-input\" placeholder=\"date\">\r\n                <span class=\"help-block\">Please enter a valid date</span>\r\n              </div>\r\n            </div> -->\r\n\r\n            <!-- <div class=\"form-group row\">\r\n              <label class=\"col-md-3 col-form-label\" for=\"disabled-input\">Disabled Input</label>\r\n              <div class=\"col-md-9\">\r\n                <input type=\"text\" id=\"disabled-input\" name=\"disabled-input\" class=\"form-control\" placeholder=\"Disabled\" disabled>\r\n              </div>\r\n            </div> -->\r\n            <!-- <div class=\"form-group row\">\r\n              <label class=\"col-md-3 col-form-label\" for=\"file-input\">File input</label>\r\n              <div class=\"col-md-9\">\r\n                <input type=\"file\" id=\"file-input\" name=\"file-input\">\r\n              </div>\r\n            </div> -->\r\n            <!-- <div class=\"form-group row\">\r\n              <label class=\"col-md-3 col-form-label\" for=\"file-multiple-input\">Multiple File input</label>\r\n              <div class=\"col-md-9\">\r\n                <input type=\"file\" id=\"file-multiple-input\" name=\"file-multiple-input\" multiple>\r\n              </div>\r\n            </div> -->\r\n          </form>\r\n        </div>\r\n        <div class=\"card-footer\" style=\"text-align: center\">\r\n          <button type=\"button\" class=\"btn btn-sm btn-primary\" (click)=\"onInfoSubmit()\"><i class=\"fa fa-dot-circle-o\"></i> Submit</button>\r\n          <button type=\"button\" class=\"btn btn-sm btn-danger\"><i class=\"fa fa-ban\"></i> Reset</button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div><!--/.row-->\r\n\r\n  <div class=\"card\">\r\n      <div class=\"card-header\">\r\n        <strong>Property File Upload</strong> \r\n      </div>\r\n      <div class=\"card-body\">\r\n        <form   [formGroup]=\"propertyUploadForm\" action=\"\" method=\"post\" class=\"form-inline\">\r\n          <!-- <div class=\"form-group\">\r\n            <label class=\"sr-only\" for=\"if-email\">Email</label>\r\n            <input type=\"email\" id=\"if-email\" name=\"if-email\" class=\"form-control\" placeholder=\"Enter Email..\" autocomplete=\"email\">\r\n          </div> -->\r\n          <div class=\"form-group\">\r\n              <div class=\"form-group\">\r\n                  <label class=\"col-md-2 col-form-label\" for=\"property\">Property</label>\r\n                  <label class=\"col-md-2 col-form-label\"></label>\r\n                  <div class=\"col-md-6\">\r\n                    <textarea type=\"property\" id=\"property\" formControlName=\"propertyData\" rows=\"10\" cols=\"60\" class=\"md-textarea form-control\" mdbInput></textarea>\r\n                  </div>\r\n                </div> \r\n          </div>\r\n        </form>\r\n      </div>\r\n      <div class=\"card-footer\" style=\"text-align: center\">\r\n        <button type=\"button\" class=\"btn btn-sm btn-primary\" (click)=\"onPropertySubmit($event)\"><i class=\"fa fa-dot-circle-o\"></i> Submit</button>\r\n        <button type=\"button\" class=\"btn btn-sm btn-danger\"><i class=\"fa fa-ban\"></i> Reset</button>\r\n      </div>\r\n    </div>\r\n    <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <strong>Upload File</strong> \r\n        </div>\r\n        <div class=\"card-body\">\r\n          <form action=\"\" method=\"post\" class=\"form-inline\">\r\n            <!-- <div class=\"form-group\">\r\n              <label class=\"sr-only\" for=\"if-email\">Email</label>\r\n              <input type=\"email\" id=\"if-email\" name=\"if-email\" class=\"form-control\" placeholder=\"Enter Email..\" autocomplete=\"email\">\r\n            </div> -->\r\n            <div class=\"form-group\">\r\n                <!-- <div class=\"form-group row\">\r\n                    <label class=\"col-md-4 col-form-label\" for=\"file-input\">Upload File</label>\r\n                    <div class=\"col-md-8\">\r\n                      <input type=\"file\" id=\"file-input\" name=\"file-input\">\r\n                    </div>\r\n                  </div> -->\r\n                  <div>\r\n                      <label>\r\n                        <input type=\"file\" #FileUpload (change)=\"selectFile($event)\" multiple>\r\n                      </label>\r\n                    </div>\r\n                 <div>\r\n                    <input type=\"button\" [disabled]=\"!selectedFiles\" (click)=\"upload()\" value=\"Upload\"/>\r\n                 </div>\r\n                 &nbsp;  &nbsp;\r\n                 <label>Uploaded File  <strong>:  {{ this.selectedFile }}</strong> </label>\r\n                <!-- <div class=\"form-group\">\r\n                    <input type=\"text\" id=\"selectedFile\" name=\"selectedFile\" class=\"form-control\" placeholder=\"{{ this.selectedFile }}\" disabled>\r\n                  </div>  -->\r\n            </div>\r\n          </form>\r\n        </div>\r\n        <!-- <div class=\"card-footer\" style=\"text-align: center\">\r\n          <button type=\"button\" class=\"btn btn-sm btn-primary\" (click)=\"upload()\"><i class=\"fa fa-upload\"></i>Upload</button>\r\n          <button type=\"button\" class=\"btn btn-sm btn-danger\"><i class=\"fa fa-ban\"></i> Reset</button>\r\n        </div> -->\r\n      </div>\r\n      <div class=\"card\">\r\n          <div class=\"card-header\">\r\n            <strong>Service Status</strong> \r\n          </div>\r\n          <div class=\"card-body\">\r\n            <form  [formGroup]=\"statusForm\" action=\"\" method=\"post\" class=\"form-inline\">\r\n                    <div class=\"form-group col-sm-4\">\r\n                      <label class=\"col-md-4 col-form-label\" for=\"status\">Status</label>\r\n                      <div class=\"col-md-8\">\r\n                      <select class=\"form-control\" formControlName=\"status\" id=\"status\">\r\n                        <option>Inactive</option>\r\n                        <option>Active</option>\r\n                      </select>\r\n                      </div>\r\n                  </div>\r\n            </form>\r\n          </div>\r\n          <div class=\"card-footer\" style=\"text-align: center\">\r\n            <button type=\"button\" class=\"btn btn-sm btn-primary\" (click)=\"onStatusSubmit()\"><i class=\"fa fa-dot-circle-o\"></i> Submit</button>\r\n            <button type=\"button\" class=\"btn btn-sm btn-danger\"><i class=\"fa fa-ban\"></i> Reset</button>\r\n          </div>\r\n        </div>\r\n</div>"

/***/ }),

/***/ "./src/app/views/theme/colors.component.ts":
/*!*************************************************!*\
  !*** ./src/app/views/theme/colors.component.ts ***!
  \*************************************************/
/*! exports provided: ColorsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ColorsComponent", function() { return ColorsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _coreui_coreui_dist_js_coreui_utilities__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @coreui/coreui/dist/js/coreui-utilities */ "./node_modules/@coreui/coreui/dist/js/coreui-utilities.js");
/* harmony import */ var _coreui_coreui_dist_js_coreui_utilities__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_coreui_coreui_dist_js_coreui_utilities__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _services_service_api_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/service-api.service */ "./src/app/services/service-api.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");






var ColorsComponent = /** @class */ (function () {
    function ColorsComponent(_document, backendApiService, router) {
        this._document = _document;
        this.backendApiService = backendApiService;
        this.router = router;
    }
    ColorsComponent.prototype.themeColors = function () {
        var _this = this;
        Array.from(this._document.querySelectorAll('.theme-color')).forEach(function (el) {
            var background = Object(_coreui_coreui_dist_js_coreui_utilities__WEBPACK_IMPORTED_MODULE_3__["getStyle"])('background-color', el);
            var table = _this._document.createElement('table');
            table.innerHTML = "\n        <table class=\"w-100\">\n          <tr>\n            <td class=\"text-muted\">HEX:</td>\n            <td class=\"font-weight-bold\">" + Object(_coreui_coreui_dist_js_coreui_utilities__WEBPACK_IMPORTED_MODULE_3__["rgbToHex"])(background) + "</td>\n          </tr>\n          <tr>\n            <td class=\"text-muted\">RGB:</td>\n            <td class=\"font-weight-bold\">" + background + "</td>\n          </tr>\n        </table>\n      ";
            el.parentNode.appendChild(table);
        });
    };
    ColorsComponent.prototype.ngOnInit = function () {
        this.themeColors();
        // this.backendApiService.getAllServices().subscribe((data)=>{
        //   console.log("calling get all service api....");
        //   this.serviceData = data['articles'];
        // });
    };
    ColorsComponent.prototype.onAddService = function () {
        console.log("service add......");
        this.router.navigate(['/theme/typography']);
    };
    ColorsComponent.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["DOCUMENT"],] }] },
        { type: _services_service_api_service__WEBPACK_IMPORTED_MODULE_4__["BackendApiService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] }
    ]; };
    ColorsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            template: __webpack_require__(/*! raw-loader!./colors.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/theme/colors.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_common__WEBPACK_IMPORTED_MODULE_2__["DOCUMENT"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _services_service_api_service__WEBPACK_IMPORTED_MODULE_4__["BackendApiService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
    ], ColorsComponent);
    return ColorsComponent;
}());



/***/ }),

/***/ "./src/app/views/theme/theme-routing.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/views/theme/theme-routing.module.ts ***!
  \*****************************************************/
/*! exports provided: ThemeRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ThemeRoutingModule", function() { return ThemeRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _colors_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./colors.component */ "./src/app/views/theme/colors.component.ts");
/* harmony import */ var _typography_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./typography.component */ "./src/app/views/theme/typography.component.ts");





var routes = [
    {
        path: '',
        data: {
            title: 'Service'
        },
        children: [
            {
                path: '',
                redirectTo: 'colors'
            },
            {
                path: 'colors',
                component: _colors_component__WEBPACK_IMPORTED_MODULE_3__["ColorsComponent"],
                data: {
                    title: 'Service List'
                }
            },
            {
                path: 'typography',
                component: _typography_component__WEBPACK_IMPORTED_MODULE_4__["TypographyComponent"],
                data: {
                    title: 'OnBoarding Service'
                }
            }
        ]
    }
];
var ThemeRoutingModule = /** @class */ (function () {
    function ThemeRoutingModule() {
    }
    ThemeRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], ThemeRoutingModule);
    return ThemeRoutingModule;
}());



/***/ }),

/***/ "./src/app/views/theme/theme.module.ts":
/*!*********************************************!*\
  !*** ./src/app/views/theme/theme.module.ts ***!
  \*********************************************/
/*! exports provided: ThemeModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ThemeModule", function() { return ThemeModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _typography_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./typography.component */ "./src/app/views/theme/typography.component.ts");
/* harmony import */ var _theme_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./theme-routing.module */ "./src/app/views/theme/theme-routing.module.ts");
/* harmony import */ var _colors_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./colors.component */ "./src/app/views/theme/colors.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");

// Angular



// Theme Routing



var ThemeModule = /** @class */ (function () {
    function ThemeModule() {
    }
    ThemeModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _theme_routing_module__WEBPACK_IMPORTED_MODULE_4__["ThemeRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ReactiveFormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
            ],
            declarations: [
                _colors_component__WEBPACK_IMPORTED_MODULE_5__["ColorsComponent"],
                _typography_component__WEBPACK_IMPORTED_MODULE_3__["TypographyComponent"]
            ]
        })
    ], ThemeModule);
    return ThemeModule;
}());



/***/ }),

/***/ "./src/app/views/theme/typography.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/views/theme/typography.component.ts ***!
  \*****************************************************/
/*! exports provided: TypographyComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TypographyComponent", function() { return TypographyComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_service_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/service-api.service */ "./src/app/services/service-api.service.ts");




var TypographyComponent = /** @class */ (function () {
    function TypographyComponent(fb, backendApiService) {
        this.fb = fb;
        this.backendApiService = backendApiService;
        this.serviceInfoCreateForm();
        this.serviceStatusCreateForm();
        this.servicePropertyUloadForm();
    }
    TypographyComponent.prototype.serviceInfoCreateForm = function () {
        this.serviceInfo = this.fb.group({
            serviceName: [''],
            serviceCode: [''],
            language: ['']
        });
    };
    TypographyComponent.prototype.serviceStatusCreateForm = function () {
        this.statusForm = this.fb.group({
            status: ['']
        });
    };
    TypographyComponent.prototype.servicePropertyUloadForm = function () {
        this.propertyUploadForm = this.fb.group({
            propertyData: ['']
        });
    };
    TypographyComponent.prototype.ngOnInit = function () {
        console.log("Inside add screen");
    };
    TypographyComponent.prototype.onInfoSubmit = function (event) {
        console.log("Inside onInfo Submit buttom");
        console.log('Your form data : ', this.serviceInfo.value);
    };
    TypographyComponent.prototype.onPropertySubmit = function (event) {
        console.log("Inside onProperty Submit buttom");
        console.log('Your form data : ', this.propertyUploadForm.value);
    };
    TypographyComponent.prototype.onFileUploadSubmit = function (event) {
        console.log("Inside onFileUpload Submit buttom");
    };
    TypographyComponent.prototype.onStatusSubmit = function (event) {
        console.log('Your form data : ', this.statusForm.value);
        console.log("Inside onStatus Submit buttom");
    };
    TypographyComponent.prototype.onAuthGenrate = function (event) {
        console.log("Inside onAuthGenrate buttom");
    };
    TypographyComponent.prototype.selectFile = function (event) {
        this.selectedFiles = event.target.files;
        console.log("files", this.selectedFiles);
    };
    TypographyComponent.prototype.upload = function () {
        var _this = this;
        console.log("Inside file upload buttom");
        var formdata = new FormData();
        for (var i = 0; i < this.selectedFiles.length; i++) {
            console.log("File NAme:.", this.selectedFiles.item(i).name);
            this.selectedFile = this.selectedFiles.item(i).name;
            formdata.append('file', this.selectedFiles.item(i), this.selectedFiles.item(i).name);
            this.backendApiService.pushFilesToStorage(this.selectedFiles.item(i)).subscribe(function (entry) {
                _this.fileUpload.nativeElement.value = '';
                _this.fileNames = entry;
                _this.selectedFiles = null;
            });
        }
    };
    TypographyComponent.ctorParameters = function () { return [
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: _services_service_api_service__WEBPACK_IMPORTED_MODULE_3__["BackendApiService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('FileUpload', { static: true }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], TypographyComponent.prototype, "fileUpload", void 0);
    TypographyComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            template: __webpack_require__(/*! raw-loader!./typography.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/theme/typography.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _services_service_api_service__WEBPACK_IMPORTED_MODULE_3__["BackendApiService"]])
    ], TypographyComponent);
    return TypographyComponent;
}());



/***/ })

}]);
//# sourceMappingURL=views-theme-theme-module.js.map